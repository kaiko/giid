-- alguses ainult mobiili app

CREATE TABLE usr
  ( id serial primary key
  , created_at timestamp(0) NOT NULL DEFAULT now()
  , secret_key varchar
  , name varchar
  , phone varchar NOT NULL UNIQUE
  , email varchar
  , about varchar
  , lang char(2)
  );

CREATE TABLE tour 
  ( id serial primary key
  , created_at timestamp(0) NOT NULL DEFAULT now()
  , deleted_at timestamp(0)
  , email varchar
  , usr integer references usr(id)
  , groupsize integer
  , at timestamp(0) 
  , until timestamp(0)
  , placeid varchar not null -- google placeid 
  , lang varchar
  );

CREATE TABLE match
  ( id serial primary key
  , created_at timestamp(0) NOT NULL DEFAULT now()
  , usr   integer NOT NULL references usr(id)
  , tour  integer NOT NULL references tour(id)
  , price integer -- cents 
  , commited_at timestamp(0)
  , rejected_at timestamp(0)
  , reject_reason varchar
  );
CREATE UNIQUE INDEX match_usr ON match(usr,tour); 

CREATE TABLE confirm_sms
  ( phone varchar NOT NULL
  , pin char(4) NOT NULL DEFAULT lpad(floor(random() * 10000.0)::text, 4, '0')
  );



CREATE TABLE postinput
  ( created_at timestamp(0) NOT NULL DEFAULT now()
  , usr integer
  , post json NOT NULL
  );
