
var zazler = require('zazler');
var pg = require('pg');

var pgConnection = 'postgresql://giid:alfa@127.0.0.1:5434/guide';

zazler.conf({
  templates: [  __dirname + "/templates/" ]
  , logs:
  [ { type: "error" , path: process.env.HOME + "/logs/error.log" }
  , { type: "sql"   , path: process.env.HOME + "/logs/sql.log" }
  , { type: "access", path: process.env.HOME + "/logs/access.log" }
  ],
});

guide = zazler.db('guide', pgConnection, {
  // which tables/fields are accessible
  "read" : "usr usr_lang usr_img tour match -usr(sess_id)",
  "write": "-*(created_at id) usr usr_lang usr_img tour match",
  "hide": "*(deleted_at)",

  // use basic authentication if needed
  "auth": { "table": "usr", "select": "id,phone,name", "where": "phone=req.user" /*:password=req.passMd5" */, realm: "Guide" }, // TODO: password

  "filters":
  [ { "table": "guide", "where": "isnull(deleted_at)" }
  , { "table": "usr", "where": "req.isPost" }
  , { "table": "usr_img", "where": "isnull(deleted_at)" }
  ], // only show not deleted images

  "alias":
  [ { name: "me", table: "usr", const: { "where.me": "id=auth.id" } }
  ],

  // "meta": { "usr_img.img": { "upload-dir": process.env.HOME + "/images", "upload-name": "ret.url" }

  "write-rules":
  [ { "table": "usr",      "on": "isnull(new.id)", "action": "insert", returning: "seq id usr_id_seq" }

  , { "table": "usr_lang", "on": "notnull(new.del)", "action": "delete", "where": "usr.id=auth.id:lang=new.lang" }
  , { "table": "usr_lang", "on": "true" , "action": "insert", "set": { "usr": "auth.id" } }

  , { "table": "usr_img", "on": "notnull(new.deleted_at)", "action": "update", "where": "usr=auth.id:id=new.id", "set": { "deleted_at": "now()" }}
  , { "table": "usr_img", "on": "isnull(new.del)" , "action": "insert" }

  , { "table": "tour", "action": "insert", "on": "isnull(new.id)", "set": { usr: "auth.id" } }
  , { "table": "tour", "action": "update", "on": "notnull(new.deleted_at)", "where": "usr=auth.id:id=new.id", "set": { deleted_at: "now()" } }
  , { "table": "tour", "action": "update", "on": "notnull(new.id)",         "where": "usr=auth.id:id=new.id" }

  , { "table": "match", "action": "insert", "on": "isnull(new.id)", "set": { "usr": "auth.id" }, returning: "seq id match_id_seq" }
  , { "table": "match", "action": "update", "on": "notnull(new.id)", "where": "id=new.id" } // TODO: auth limitation
  ]
});

pg.connect(pgConnection, function (err, client, done) {
  guide.on('data-post', function (cb,ev) {
    client.query("INSERT INTO postinput(usr, post) VALUES ($1,$2)", [ev.req.user, JSON.stringify( ev.req.post ) ], function (err, result) { if (err) console.log(err) });
    cb();
  })
});

zazler.listen(9000);
// now access http://127.0.0.1:9000/guide/


/*
bash:
GDATA="http://127.0.0.1:9000/guide/"
# GDATA="http://guide.zazler.net/"
p()  { curl -X POST -H "Content-Type: application/json" -d "$2"       "$GDATA$1.json"; }
pu() { curl -X POST -H "Content-Type: application/json" -d "$3" -u $1 "$GDATA$2.json"; }

# kasutaja tekitamine
p usr '[{"name": "Kaiko Kaur", "phone": "37255655325", "lang": "et"}]'
p usr '[{"name": "Some Guide", "phone": "37255555555", "lang": "en"}]'

# Tuuri lisamine
pu 37255655325:_ tour '[{"groupsize": 4, "at": "2016-04-01 12:00:00", "until": "2016-04-01 14:00:00", "lang": "et", "placeid": "ChIJH0lPaPw260YRNGUbihXFXeE"}]'

# Tuuri uuendamine
pu 37255655325:_ tour '[{"id": 1, "groupsize": 3}]'

# Tuuri tühistamine
pu 37255655325:_ tour '[{"id": 1, "deleted_at": true}]'

# Giidi poolne päringu tegemine
curl $GDATA/tour.json

# Giidi poolne pakkumise tegemine
pu 37255555555:_ match '[{"tour": 1, "price": 12000 }]'

# Pakkumise kinnitamine
pu 37255655325:_ match '[{"id": 1, "commited_at": "2016-01-01 12:12:12" }]'

# Pakkumise tagasilükkamine
pu 37255655325:_ match '[{"id": 1, "rejected_at": "2016-01-01 12:12:12", reject_reason: "Something ..." }]'
*/
